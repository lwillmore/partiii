package com.bwinnovations.text.textpip.iepipe;

import java.io.IOException;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.bwinnovations.text.textpipe.MissingRequiredAnnotationException;
import com.bwinnovations.text.textpipe.ieannotations.BinaryRelation;
import com.bwinnovations.text.textpipe.iepipe.ReverbIEPipe;
import com.bwinnovations.text.textpipe.nlpannotation.Document;
import com.bwinnovations.text.textpipe.nlpannotation.Token;
import com.bwinnovations.text.textpipe.nlppipe.OpenNLPSentencePipe;
import com.bwinnovations.text.textpipe.nlppipe.OpenNLPTokenPipe;

/**
 * Tests for the IEPipes functionality
 * @author Laurence Willmore
 *
 */
public class IEPipeTests {

	/**
	 * Do this before the tests
	 */
	@Before
	public void setUp(){

	}
	
	/**
	 * Basic test of the {@link ReverbIEPipe}
	 */
	@Test
	public void BasicReverbIEPipeTest(){
		String input = "Richard Branson owns Virgin Media. Laurence Willmore is the king of the world.";
		String[][] groundtruth = new String[][]{
				new String[]{"Richard Branson", "owns", "Virgin Media"},
				new String[]{"Laurence Willmore", "is the king of", "the world"}
		};
		Document doc = new Document(input);
		ReverbIEPipe revpipe = null;
		OpenNLPSentencePipe sentpipe = new OpenNLPSentencePipe();
		OpenNLPTokenPipe tokPipe = new OpenNLPTokenPipe();
		try {
			revpipe = new ReverbIEPipe();	
		} catch (IOException e) {			
			e.printStackTrace();
		}
		try {
			sentpipe.annotate(doc);
			tokPipe.annotate(doc);
			revpipe.annotate(doc);
		} catch (MissingRequiredAnnotationException e1) {
			e1.printStackTrace();
		}
		
		List<BinaryRelation> relations = doc.getAnnotationsFor(BinaryRelation.class);
		for(int i = 0; i < relations.size();i++){
			String[] parts = groundtruth[i];
			BinaryRelation relation = relations.get(i);
			String ent1 = relation.getEntity1().getTextMatched();		
			String ent2 = relation.getEntity2().getTextMatched();
			List<Token> relToks = relation.getRelation().getTokens();
			int s = relToks.get(0).getStart();
			int e = relToks.get(relToks.size()-1).getStop();
			String rel = doc.getText().substring(s, e);
			Assert.assertTrue(parts[1].equals(rel));
			Assert.assertTrue(parts[0].equals(ent1));
			Assert.assertTrue(parts[2].equals(ent2));
		}
	}

}
