package com.bwinnovations.fileutils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * General utility methods for files
 * @author Laurence Willmore
 *
 */
public class FileUtils {
	
	/**
	 * Gets the contents of a file as a string
	 * @param inFile
	 * @return
	 * @throws IOException
	 */
	public static String getStringFrom(File inFile) throws IOException {
		FileInputStream stream = new FileInputStream(inFile);
		try {
			FileChannel fc = stream.getChannel();
			MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0,
					fc.size());
			/* Instead of using default, pass in a decoder. */
			return Charset.defaultCharset().decode(bb).toString();
		} finally {
			stream.close();
		}
	}

}
