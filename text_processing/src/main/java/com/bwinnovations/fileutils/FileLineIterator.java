package com.bwinnovations.fileutils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

public class FileLineIterator implements Iterator<String> {

	private BufferedReader reader;
	private String strLine;
	DataInputStream in;

	public FileLineIterator(String filepath) {
		try {
			// Open the file that is the first
			// command line parameter
			FileInputStream fstream = new FileInputStream(filepath);
			// Get the object of DataInputStream
			in = new DataInputStream(fstream);
			reader = new BufferedReader(new InputStreamReader(in));
			String strLine = reader.readLine();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}

	@Override
	public boolean hasNext() {
		if (strLine == null) {
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
		return true;
	}

	@Override
	public String next() {
		String s = strLine;
		try {
			strLine = reader.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub

	}

}
