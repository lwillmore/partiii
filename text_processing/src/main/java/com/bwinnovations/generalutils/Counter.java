package com.bwinnovations.generalutils;

public class Counter {
	
	private int count;

	public Counter(int start) {
		super();
		count = start;
	}
	
	public void inc(){
		count++;
	}
	
	public int getCount(){
		return count;
	}
	
	public void add(int addThis){
		count+=addThis;
	}
	
	

}
