package com.bwinnovations.text.textpipe.nlpannotation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bwinnovations.text.textpipe.AnnotationInterface;
import com.bwinnovations.text.textpipe.CompoundAnnotationInterface;

public abstract class DocumentCompoundAnnotation<ANNOTATE_WITH, COMP_ANNO> extends DocumentAnnotation<ANNOTATE_WITH> implements CompoundAnnotationInterface<ANNOTATE_WITH,COMP_ANNO>{

	protected HashMap<Class<? extends COMP_ANNO>, List<COMP_ANNO>> subAnnotations;

	
	public DocumentCompoundAnnotation(Document doc) {
		super(doc);
		subAnnotations = new HashMap<Class<? extends COMP_ANNO>, List<COMP_ANNO>>();
	} 

	@Override
	public void addSubAnnotation(COMP_ANNO annotation) {
		if (subAnnotations.containsKey(annotation.getClass())) {
			subAnnotations.get(annotation.getClass()).add(annotation);
		} else {
			List<COMP_ANNO> annos = new ArrayList<COMP_ANNO>();
			annos.add(annotation);
			subAnnotations.put((Class<? extends COMP_ANNO>) annotation.getClass(), annos);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends COMP_ANNO> List<T> getSubAnnotationsFor(
			Class<T> key) {
		if (subAnnotations.containsKey(key)) {
			return (List<T>) subAnnotations.get(key);
		}
		return null;

	}

}
