package com.bwinnovations.text.textpipe.nlppipe;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;


import com.bwinnovations.text.textpipe.MissingRequiredAnnotationException;
import com.bwinnovations.text.textpipe.nlpannotation.Document;
import com.bwinnovations.text.textpipe.nlpannotation.DocumentAnnotationInterface;
import com.bwinnovations.text.textpipe.nlpannotation.Sentence;

/**
 * A Sentence Annotator that is backed by Apache's OpenNLP.
 * @author Laurence Willmore
 *
 */
public class OpenNLPSentencePipe extends NLPPipeSection{
	
	/**
	 * Property name pointing to the sentence model
	 */
	public static final String SENTENCE_MODEL_PROP = "com"+File.separator+"bwinnovations"+File.separator+"text"+File.separator+"opennlp"+File.separator+"models"+File.separator+"en-sent.bin";
	SentenceDetectorME sentenceDetector;

	/**
	 *
	 */
	public OpenNLPSentencePipe(){
		super();
		InputStream modelIn = null;
		modelIn = this.getClass().getClassLoader().getResourceAsStream(SENTENCE_MODEL_PROP);
		SentenceModel model=null;
		try {
		  model = new SentenceModel(modelIn);
		}
		catch (IOException e) {
		  e.printStackTrace();
		}
		finally {
		  if (modelIn != null) {
		    try {
		      modelIn.close();
		    }
		    catch (IOException e) {
		    }
		  }
		}
		sentenceDetector = new SentenceDetectorME(model);
	}

	@Override
	public void performAnnotation(Document document)
			throws MissingRequiredAnnotationException {
		ArrayList<Sentence> sents = new ArrayList<Sentence>();
		List<String> sentences = Arrays.asList(sentenceDetector.sentDetect(document.getText()));
		int currentOff =0;
		for(int i =0; i<sentences.size();i++){
			String sentence = sentences.get(i);
			int start=currentOff+(document.getText().substring(currentOff).indexOf(sentence));
			int stop=start+sentence.length();
			sents.add(new Sentence(document,start,stop));
		}
		document.addAllAnnotations((Collection<? extends DocumentAnnotationInterface<?>>) sents);
	}

	@Override
	public void checkForRequiredAnnotations(Document annotation)
			throws MissingRequiredAnnotationException {
		// None required
		
	}
	
	
	/**
	 * Quick test main
	 * @param args
	 */
	public static void main(String[] args) {
		OpenNLPSentencePipe pipe = new OpenNLPSentencePipe();
		Document doc = new Document("This is sentence one. This is sentence two.");
		try {
			pipe.annotate(doc);
			for(Sentence sent: doc.getAnnotationsFor(Sentence.class)){
				System.out.println("1) "+ sent.getTextMatched());
			}
		} catch (MissingRequiredAnnotationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
