package com.bwinnovations.text.textpipe.nlpannotation;

import com.bwinnovations.text.textpipe.AnnotationInterface;
import edu.mit.jwi.item.POS;

/**
 * A {@link TokenAnnotationInterface} that records the Penn Treebank part of speech for a
 * token.
 * 
 * @author Laurence Willmore
 * 
 */
public class TreeBankPOS extends TokenSingletonAnnotation<AnnotationInterface<?>> {

	PartOfSpeech pos;

	/**
	 * Constructor on {@link Token} and the {@link PartOfSpeech}
	 * @param token
	 * @param pos
	 */
	public TreeBankPOS(Token token, PartOfSpeech pos) {
		super(token);
		this.pos = pos;
	}

	/**
	 * @return
	 */
	public PartOfSpeech getPos() {
		return pos;
	}

	/**
	 * Penn Treebank part of speech types.
	 * 
	 * @author Laurence Willmore
	 * 
	 */
	public enum PartOfSpeech {
		@SuppressWarnings("javadoc")
		CC("Coordinating conjunction"), @SuppressWarnings("javadoc")
		CD("Cardinal number"), @SuppressWarnings("javadoc")
		DT("Determiner"), @SuppressWarnings("javadoc")
		EX("Existential there"), @SuppressWarnings("javadoc")
		FW("Foreign word"), @SuppressWarnings("javadoc")
		IN("Preposition or subordinating conjunction"), @SuppressWarnings("javadoc")
		JJ("Adjective"), @SuppressWarnings("javadoc")
		JJR("Adjective, comparative"), @SuppressWarnings("javadoc")
		JJS("Adjective, superlative"), @SuppressWarnings("javadoc")
		LS("List item marker"), @SuppressWarnings("javadoc")
		MD("Modal"), @SuppressWarnings("javadoc")
		NN("Noun, singular or mass"), @SuppressWarnings("javadoc")
		NNS("Noun, plural"), @SuppressWarnings("javadoc")
		NNP("Proper noun, singular"), @SuppressWarnings("javadoc")
		NNPS("Proper noun, plural"), @SuppressWarnings("javadoc")
		PDT("Predeterminer"), @SuppressWarnings("javadoc")
		POS("Possessive ending"), @SuppressWarnings("javadoc")
		PRP("Personal pronoun"), @SuppressWarnings("javadoc")
		PRP$("Possessive pronoun (prolog version PRP-S)"), @SuppressWarnings("javadoc")
		RB("Adverb"), @SuppressWarnings("javadoc")
		RBR("Adverb, comparative"), @SuppressWarnings("javadoc")
		RBS("Adverb, superlative"), @SuppressWarnings("javadoc")
		RP("Particle"), @SuppressWarnings("javadoc")
		SYM("Symbol"), @SuppressWarnings("javadoc")
		TO("to"), @SuppressWarnings("javadoc")
		UH("Interjection"), @SuppressWarnings("javadoc")
		VB("Verb, base form"), @SuppressWarnings("javadoc")
		VBD("Verb, past tense"), @SuppressWarnings("javadoc")
		VBG("Verb, gerund or present participle"), @SuppressWarnings("javadoc")
		VBN("Verb, past participle"), @SuppressWarnings("javadoc")
		VBP("Verb, non-3rd person singular present"), @SuppressWarnings("javadoc")
		VBZ("Verb, 3rd person singular present"), @SuppressWarnings("javadoc")
		WDT("Wh-determiner"), @SuppressWarnings("javadoc")
		WP("Wh-pronoun"), @SuppressWarnings("javadoc")
		WP$("Possessive wh-pronoun (prolog version WP-S)"), @SuppressWarnings("javadoc")
		WRB("Wh-adverb"),
		/**
		 * This is added for pos with no mapping to Penn Treebank.
		 */
		UK("Unknown");

		/**
		 * Penn Tree Bank short description
		 */
		public final String DESCRIPTION;

		PartOfSpeech(String description) {
			this.DESCRIPTION = description;
		}

		/**
		 * Gets a {@link PartOfSpeech} from a string.
		 * 
		 * @param pennAbreviation
		 * @return PartOfSpeech
		 */
		public static PartOfSpeech getPOSfromString(String pennAbreviation) {
			for (PartOfSpeech pos : PartOfSpeech.values()) {
				if (pos.toString().equals(pennAbreviation))
					return pos;
			}
			return PartOfSpeech.UK;
		}
		
		public POS toWordnetPOS(){
			String name = this.name();
			if(name.startsWith("N"))return edu.mit.jwi.item.POS.NOUN;
			if(name.startsWith("V"))return edu.mit.jwi.item.POS.VERB;
			if(name.startsWith("J"))return edu.mit.jwi.item.POS.ADJECTIVE;
			if(name.startsWith("RB"))return edu.mit.jwi.item.POS.ADVERB;
			return null;
		}
	};
}
