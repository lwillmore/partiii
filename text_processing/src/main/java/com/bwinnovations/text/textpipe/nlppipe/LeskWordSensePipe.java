package com.bwinnovations.text.textpipe.nlppipe;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.bwinnovations.text.textpipe.AnnotationInterface;
import com.bwinnovations.text.textpipe.MissingRequiredAnnotationException;
import com.bwinnovations.text.textpipe.nlpannotation.Document;
import com.bwinnovations.text.textpipe.nlpannotation.Sentence;
import com.bwinnovations.text.textpipe.nlpannotation.Token;
import com.bwinnovations.text.textpipe.nlpannotation.TreeBankPOS;
import com.bwinnovations.text.textpipe.nlpannotation.WordnetSense;
import com.bwinnovations.text.wordnet.DictionaryDimensions;
import com.bwinnovations.text.wordnet.WordnetUtils;

import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.POS;

/**
 * An annotator for wordsense, using the Lesk algorithm (as described in
 * Jurafsky and martin) and Wordnet senses.
 * 
 * @author Laurence Willmore
 * 
 */
public class LeskWordSensePipe extends NLPPipeSection {

	protected DictionaryDimensions dd;
	OpenNLPSentencePipe sentPipe;
	OpenNLPTokenPipe tokPipe;
	OpenNLPPOSPipe posPipe;

	/**
	 * Default constructor
	 * 
	 * @param dictionaryPath
	 */
	public LeskWordSensePipe(DictionaryDimensions dd) {
		super();
		this.dd = dd;
		sentPipe = new OpenNLPSentencePipe();
		tokPipe = new OpenNLPTokenPipe();
		posPipe = new OpenNLPPOSPipe();
	}

	@Override
	public void performAnnotation(Document annotation)
			throws MissingRequiredAnnotationException {
		for (Sentence sentence : annotation.getAnnotationsFor(Sentence.class)) {
			for (Token token : sentence.getAnnotationsFor(Token.class)) {
				annotate(token, sentence);
			}
		}
	}

	protected void annotate(Token token, Sentence sentence) {
		String tokenString = token.getTextMatched();
		POS pos = token.getAnnotationsFor(TreeBankPOS.class).get(0).getPos()
				.toWordnetPOS();
		if (pos != null) {
			List<String> stems = dd.stemmer.findStems(token.getTextMatched(),
					pos);
			if (!stems.isEmpty()) {
				List<String> context = getStemmedStringList(sentence
						.getAnnotationsFor(Token.class));
				while(context.contains(stems.get(0)))context.remove(stems.get(0));
				/**
				 * This is the Lesk algorithm
				 */
				List<ISynset> possibleSynsets = dd.getPossibleSynsets(
						stems.get(0), pos);

				ISynset bestSense = possibleSynsets.get(0);

				int maxOverlap = 0;

				for (ISynset sense : possibleSynsets) {
					List<String> signature = getStemmedStringList(getSignatureWords(sense));
					while(signature.contains(stems.get(0)))signature.remove(stems.get(0));
					String sigString = StringUtils.join(signature," ");
					int overlap = computeOverlap(signature, context);
					if (overlap > maxOverlap) {
						bestSense = sense;
						maxOverlap = overlap;
					}
				}
				token.addAnnotation(new WordnetSense(token, bestSense));
			}
		}
	}

	/**
	 * Gets an annotated sentence from the Gloss of the synset and other stuff
	 * if possible.
	 * 
	 * @param sense
	 * @return
	 */
	protected List<Token> getSignatureWords(ISynset sense) {
		Document d = new Document(sense.getGloss());
		try {
			sentPipe.annotate(d);
			tokPipe.annotate(d);
			posPipe.annotate(d);
		} catch (MissingRequiredAnnotationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return d.getAnnotationsFor(Token.class);
	}

	/**
	 * Return the list of stem Strings that are stemmable (also comparable in
	 * wordnet)
	 * 
	 * @param sentence
	 * @return
	 */
	protected List<String> getStemmedStringList(List<Token> in) {
		List<String> result = new ArrayList<String>();
		for (Token word : in) {
			List<String> stems = dd.stemmer.findStems(word.getTextMatched(),
					word.getAnnotationsFor(TreeBankPOS.class).get(0).getPos()
							.toWordnetPOS());
			if (!stems.isEmpty())
				result.add(stems.get(0));
		}
		return result;
	}

	/**
	 * Part of Lesk algorithm. How many words are in common between the
	 * signature and the context?
	 * 
	 * @param signature
	 * @param context
	 * @return
	 */
	protected int computeOverlap(List<String> signature, List<String> context) {
		int count = 0;
		for(String x : signature){
			for(String y :context){
				if(x.toLowerCase().equals(y.toLowerCase())){
					count++;			
				}
			}
		}
		return count;
	}

	@Override
	public void checkForRequiredAnnotations(Document annotation)
			throws MissingRequiredAnnotationException {
		List<Class<? extends AnnotationInterface>> missing = new ArrayList<Class<? extends AnnotationInterface>>();
		if (!annotation.getAnnotationKeyList().contains(Sentence.class))
			missing.add(Sentence.class);
		if (!annotation.getAnnotationKeyList().contains(Token.class))
			missing.add(Token.class);
		if (!annotation.getAnnotationKeyList().contains(TreeBankPOS.class))
			missing.add(Token.class);
		if (missing.size() > 0) {
			throw new MissingRequiredAnnotationException(
					"LeskWordSensePipe is missing required Annotations on the Document",
					missing);
		}
	}

}
