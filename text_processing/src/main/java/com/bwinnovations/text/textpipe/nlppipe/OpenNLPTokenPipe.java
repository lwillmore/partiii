package com.bwinnovations.text.textpipe.nlppipe;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.tokenize.TokenizerME;

import com.bwinnovations.text.textpipe.MissingRequiredAnnotationException;
import com.bwinnovations.text.textpipe.nlpannotation.Document;
import com.bwinnovations.text.textpipe.nlpannotation.Token;

/**
 * An apache openNLP tokeniser.
 * @author Laurence Willmore
 *
 */
public class OpenNLPTokenPipe extends TokenPipe{
	
	TokenizerME tokenizer;
	/**
	 * Resource path to model.
	 */
	public static final String TOKEN_MODEL_PROP = "com"+File.separator+"bwinnovations"+File.separator+"text"+File.separator+"opennlp"+File.separator+"models"+File.separator+"en-token.bin";
	
	/**
	 * Default constructor
	 */
	public OpenNLPTokenPipe(){
		super();
		InputStream modelIn=null;
		modelIn = this.getClass().getClassLoader().getResourceAsStream(TOKEN_MODEL_PROP);
		TokenizerModel model = null;
		try {
			model = new TokenizerModel(modelIn);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (modelIn != null) {
				try {
					modelIn.close();
				} catch (IOException e) {
				}
			}
		}
		tokenizer = new TokenizerME(model);
	}

	@Override
	List<Token> tokenise(String text, int offset, Document doc) {
		List<Token> tla = new ArrayList<Token>();
		int currentOff =0;
		for(String token : tokenizer.tokenize(text)){
			int start = currentOff+(text.substring(currentOff).indexOf(token));
			int stop = start+token.length();
			tla.add(new Token(doc, offset+start, offset+stop));
			currentOff = stop;
		}
		return tla;
	}

	@Override
	public void checkForRequiredAnnotations(Document annotation)
			throws MissingRequiredAnnotationException {
		//None required
	}

}
