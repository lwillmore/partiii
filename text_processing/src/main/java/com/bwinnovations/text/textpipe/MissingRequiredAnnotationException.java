package com.bwinnovations.text.textpipe;

import java.util.List;

/**
 * Exception that should be thrown by a {@link PipeSection} if a required
 * {@link AnnotationInterface} is not found.
 * 
 * @author Laurence Willmore
 * 
 */
public class MissingRequiredAnnotationException extends Exception {

	private static final long serialVersionUID = 7979888932414909572L;

	private List<Class<? extends AnnotationInterface>> missingAnnotations;

	/**
	 * Construct with message and list of missing {@link AnnotationInterface}
	 * @param message
	 * @param missingAnnotations
	 */
	public MissingRequiredAnnotationException(String message,
			List<Class<? extends AnnotationInterface>> missingAnnotations) {
		super(message);
		this.missingAnnotations = missingAnnotations;
	}

	/**
	 * @return
	 */
	public List<Class<? extends AnnotationInterface>> getMissingAnnotations() {
		return missingAnnotations;
	}
}
