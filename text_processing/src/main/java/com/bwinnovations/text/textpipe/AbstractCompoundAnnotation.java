package com.bwinnovations.text.textpipe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * An abstract implementation of {@link CompoundAnnotationInterface}
 * 
 * @author Laurence Willmore
 * @param <ANNOTATE_WITH> Super-type of all annotations that may annotate this annotation.
 * @param <COMP_ANNO> Super-Type of the annotations that can be a part of this compound annotation
 * 
 */
public abstract class AbstractCompoundAnnotation<ANNOTATE_WITH,COMP_ANNO> implements
		CompoundAnnotationInterface<ANNOTATE_WITH, COMP_ANNO> {

	private HashMap<Class<? extends COMP_ANNO>, List<COMP_ANNO>> subAnnotations;
	private HashMap<Class<? extends ANNOTATE_WITH>, List<ANNOTATE_WITH>> annotations;
	private double confidence;

	/**
	 * Default Constructor
	 */
	public AbstractCompoundAnnotation() {
		annotations = new HashMap<Class<? extends ANNOTATE_WITH>, List<ANNOTATE_WITH>>();
		subAnnotations = new HashMap<Class<? extends COMP_ANNO>, List<COMP_ANNO>>();
	}

	/*------------------------------AnnotationInterface Methods----------------------*/

	@SuppressWarnings("unchecked")
	public  void addAnnotation(ANNOTATE_WITH annotation) {
		if (annotations.containsKey(annotation.getClass())) {
			annotations.get(annotation.getClass()).add(annotation);
		} else {
			ArrayList<ANNOTATE_WITH> annos = new ArrayList<ANNOTATE_WITH>();
			annos.add(annotation);
			annotations.put((Class<? extends ANNOTATE_WITH>) annotation.getClass(), annos);
		}
	}

	public void addAllAnnotations(
			Collection<? extends ANNOTATE_WITH> annotationCollection) {
		if (annotationCollection != null && annotationCollection.size() > 0) {
			@SuppressWarnings("unchecked")
			Class<? extends ANNOTATE_WITH> key = (Class<? extends ANNOTATE_WITH>)annotationCollection
					.iterator().next().getClass();
			if (annotations.containsKey(key)) {
				annotations.get(key).addAll(annotationCollection);
			} else {
				ArrayList<ANNOTATE_WITH> annos = new ArrayList<ANNOTATE_WITH>();
				annos.addAll(annotationCollection);
				annotations.put(key, annos);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends ANNOTATE_WITH> List<T> getAnnotationsFor(
			Class<T> key) {
		if (annotations.containsKey(key)) {
			return (List<T>) annotations.get(key);
		}
		return null;
	}

	public Set<Class<? extends ANNOTATE_WITH>> getAnnotationKeyList() {
		return annotations.keySet();
	}

	public double getConfidence() {
		return confidence;
	}

	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}

	public <T extends ANNOTATE_WITH> List<T> getSubSeqence(Class<T> key,
			int start, int stop) {
		if (this.hasAnnotation(key)) {
			List<T> s = this.getAnnotationsFor(key);
			if (start >= 0 && stop >= start && start < s.size()
					&& stop < s.size()) {
				return s.subList(start, stop);
			}
		}
		return null;
	}

	public <T extends ANNOTATE_WITH> T getAnnotationAtIndex(Class<T> key,
			int index) {
		if (this.hasAnnotation(key)) {
			List<T> s = this.getAnnotationsFor(key);
			if (index >= 0 && index < s.size()) {
				return s.get(index);
			}
		}
		return null;
	}

	public boolean hasAnnotation(
			Class<? extends ANNOTATE_WITH> annotationClass) {
		if (getAnnotationKeyList().contains(annotationClass))
			return true;
		return false;
	}

	/*------------------------------CompoundAnnotationInterface Methods----------------------*/
	
	@SuppressWarnings("unchecked")
	public void addSubAnnotation(COMP_ANNO annotation) {
		if (subAnnotations.containsKey(annotation.getClass())) {
			subAnnotations.get(annotation.getClass()).add(annotation);
		} else {
			List<COMP_ANNO> annos = new ArrayList<COMP_ANNO>();
			annos.add(annotation);
			subAnnotations.put((Class<? extends COMP_ANNO>) annotation.getClass(), annos);
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends COMP_ANNO> List<T> getSubAnnotationsFor(
			Class<T> key) {
		if (subAnnotations.containsKey(key)) {
			return (List<T>) subAnnotations.get(key);
		}
		return null;
	}

}
