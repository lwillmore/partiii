package com.bwinnovations.text.textpipe;

import java.util.List;

/**
 * {@link CompoundAnnotationInterface} provides the ability to build annotations from multiple annotations.
 * 
 * @author Laurence Willmore
 * 
 */
public interface CompoundAnnotationInterface<ANNOTATE_WITH, COMP_ANNO> extends AnnotationInterface<ANNOTATE_WITH> {

	void addSubAnnotation(COMP_ANNO annotation);

	<T extends COMP_ANNO> List<T> getSubAnnotationsFor(Class<T> key);

}
