package com.bwinnovations.text.textpipe.nlppipe;

import com.bwinnovations.text.textpipe.PipeSection;
import com.bwinnovations.text.textpipe.nlpannotation.Document;

/**
 * A {@link PipeSection} that annotates {@link Document} annotations.
 * @author Laurence Willmore
 *
 */
public abstract class NLPPipeSection extends PipeSection<Document>{

}
