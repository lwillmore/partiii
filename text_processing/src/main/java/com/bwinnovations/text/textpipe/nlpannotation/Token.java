package com.bwinnovations.text.textpipe.nlpannotation;


/**
 * A token annotation.
 * @author Laurence Willmore
 *
 */
public class Token extends DocumentSegmentAnnotation<TokenAnnotationInterface<?>>{

	/**
	 * Constructor from {@link DocumentSegmentAnnotation}
	 * @param root
	 * @param start
	 * @param stop
	 */
	public Token(Document root, int start, int stop) {
		super(root, start, stop);
	}

}
