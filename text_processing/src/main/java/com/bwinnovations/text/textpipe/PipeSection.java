package com.bwinnovations.text.textpipe;

/**
 * {@link PipeSection}s annotate {@link AnnotationInterface}s.
 * @author Laurence Willmore
 *
 * @param <INPUT_ANNOTATION>
 */
public abstract class PipeSection<INPUT_ANNOTATION extends AnnotationInterface> {
	
	/**
	 * Annotates the given {@link Annotation}. The work is delegated to the abstract methods.
	 * @param annotation
	 * @throws MissingRequiredAnnotationException
	 */
	public void  annotate(INPUT_ANNOTATION annotation) throws MissingRequiredAnnotationException{
		checkForRequiredAnnotations(annotation);
		performAnnotation(annotation);
	}
	
	/**
	 * This is where the actual annotation needs to happen.
	 * @param annotation
	 * @throws MissingRequiredAnnotationException
	 */
	public abstract void performAnnotation(INPUT_ANNOTATION annotation) throws MissingRequiredAnnotationException;
	
	/**
	 * This is where any additional checks for other required annotations can be done.
	 * @param annotation
	 * @throws MissingRequiredAnnotationException
	 */
	public abstract void checkForRequiredAnnotations(INPUT_ANNOTATION annotation) throws MissingRequiredAnnotationException;


}
