package com.bwinnovations.text.textpipe.nlpannotation;

import java.util.List;

/**
 * An annotation on {@link Token} annotations with underlying implied {@link DocumentSegmentAnnotation}.
 * @author Laurence Willmore
 *
 */
public interface TokenAnnotationInterface<ANNOTATE_WITH> extends DocumentAnnotationInterface<ANNOTATE_WITH>{

	public List<Token> getTokens();

}
