package com.bwinnovations.text.textpipe.nlppipe;

import java.util.List;

import com.bwinnovations.text.textpipe.MissingRequiredAnnotationException;
import com.bwinnovations.text.textpipe.nlpannotation.Document;
import com.bwinnovations.text.textpipe.nlpannotation.Sentence;
import com.bwinnovations.text.textpipe.nlpannotation.Token;

/**
 * An abstract {@link NLPPipeSection} that formalises how {@link Token}
 * annotations should be added to a {@link Document}
 * 
 * @author Laurence Willmore
 * 
 */
public abstract class TokenPipe extends NLPPipeSection {

	@Override
	public void performAnnotation(Document document)
			throws MissingRequiredAnnotationException {
		if (document.getAnnotationKeyList().contains(Sentence.class)) {
			for (Sentence sentence : document.getAnnotationsFor(Sentence.class)) {
				List<Token> tokens = tokenise(sentence.getTextMatched(),
						sentence.getStart(), document);
				sentence.addAllAnnotations(tokens);
				document.addAllAnnotations(tokens);
			}
		} else
			document.addAllAnnotations(tokenise(document.getText(), 0, document));
	}

	/**
	 * This is where the tokenise work is done for an extending class.
	 * 
	 * @param text
	 *            - The String to be tokenised
	 * @param offset
	 *            - The offset that should be added to each {@link Token} start
	 *            and stop. (text may be a sentence midway through the Document)
	 * @param doc
	 *            - The parent Document.
	 * @return
	 */
	abstract List<Token> tokenise(String text, int offset, Document doc);

}
