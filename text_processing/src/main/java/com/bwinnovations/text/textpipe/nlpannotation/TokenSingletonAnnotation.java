package com.bwinnovations.text.textpipe.nlpannotation;

import java.util.ArrayList;
import java.util.List;

public abstract class TokenSingletonAnnotation<ANNOTATE_WITH> extends TokenSequenceAnnotation<ANNOTATE_WITH>{
	
	protected Token token;

	public TokenSingletonAnnotation(Token token) {
		super(token.getDocument(),token.getStart(), token.getStop());
		this.token=token;
	}

	@Override
	public List<Token> getTokens() {
		return tokens;
	}
	
	public Token getToken() {
		return token;
	}

}
