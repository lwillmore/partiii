package com.bwinnovations.text.textpipe.nlpannotation;

import com.bwinnovations.text.textpipe.AbstractAnnotation;
import com.bwinnovations.text.textpipe.AnnotationInterface;

/**
 * Any annotation on a {@link Document}
 * @author Laurence Willmore
 *
 */
public interface DocumentAnnotationInterface<ANNOTATE_WITH> extends AnnotationInterface<ANNOTATE_WITH>{
	
	/**
	 * Returns the {@link Document} of this annotation.
	 * @return
	 */
	public Document getDocument();

}
