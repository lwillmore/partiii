package com.bwinnovations.text.textpipe.nlpannotation;

import com.bwinnovations.text.textpipe.AbstractAnnotation;

/**
 * An annotation on any {@link Document} that represents a sequential segment of characters.
 * It provides the basic String matches for the annotation.
 * @author Laurence Willmore
 *
 */
public abstract class DocumentSegmentAnnotation<ANNOTATE_WITH> extends DocumentAnnotation<ANNOTATE_WITH> {
	
	protected int start;
	protected int stop;

	/**
	 * Constructor sets the int of the start and stop chars in the root
	 * {@link Document} String
	 * 
	 * @param root
	 * @param start start char inclusive
	 * @param stop	stop char exclusive
	 */
	public DocumentSegmentAnnotation(Document root, int start, int stop) {
		super(root);
		this.start = start;
		this.stop = stop;
	}

	/**
	 * Gets the string matched by the annotation.
	 * @return
	 */
	public String getTextMatched() {
		return root.getText().substring(start, stop);
	}

	/**
	 * Return the int of the start char inclusive
	 * @return
	 */
	public int getStart() {
		return start;
	}

	/**
	 * Return the int of the stop char exclusive
	 * @return
	 */
	public int getStop() {
		return stop;
	}	
	
	
}
