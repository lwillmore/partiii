package com.bwinnovations.text.textpipe.ieannotations;

import java.util.List;

import com.bwinnovations.text.textpipe.AnnotationInterface;
import com.bwinnovations.text.textpipe.nlpannotation.Document;
import com.bwinnovations.text.textpipe.nlpannotation.Token;
import com.bwinnovations.text.textpipe.nlpannotation.TokenCompoundAnnotation;

public class Relation extends TokenCompoundAnnotation<AnnotationInterface<?>>{

	public Relation(Document doc, List<Token> tokens) {
		super(doc, tokens);
	}

}
