package com.bwinnovations.text.textpipe.nlpannotation;

import java.util.ArrayList;
import java.util.List;

import com.bwinnovations.text.textpipe.AbstractAnnotation;

/**
 * Document is the base annotation for TextPipe. It is text that can take
 * further annotations.
 * 
 * @author Laurence Willmore
 * 
 */
public class Document extends
		AbstractAnnotation<DocumentAnnotationInterface<?>> {

	private String text;

	/**
	 * Constructor from String
	 * 
	 * @param text
	 */
	public Document(String text) {
		super();
		this.text = text;
	}

	/**
	 * Returns the raw text of the document
	 * 
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * Gets all annotations of type key that lie exclusively in the range of
	 * characters indicated by start and stop.
	 * 
	 * @param key
	 * @param start
	 * @param stop
	 * @return
	 */
	public <T extends DocumentSegmentAnnotation<?>> List<T> getAnnotationsInCharRangeExclusive(
			Class<T> key, int start, int stop) {
		ArrayList<T> result = new ArrayList<T>();
		if (start >= 0 && stop >= start && start < text.length()
				&& stop < text.length() && this.hasAnnotation(key)) {
			List<T> s = this.getAnnotationsFor(key);
			boolean startFound = false;
			for (T anno : s) {
				if (!startFound) {
					if (anno.getStart() > start || anno.getStop() > stop)
						break;
					else {
						startFound = true;
						result.add(anno);
						continue;
					}
				} else {
					if (anno.getStart() > stop || anno.getStop() > stop)
						return result;
					else {
						result.add(anno);
						continue;
					}
				}
			}
		}
		return result;
	}

	/**
	 * Returns the annotation of type key that encompasses the char at position
	 * loc.
	 * 
	 * @param key - Annotation class to look for
	 * @param loc - character index that the annotation encompasses
	 * @return
	 */
	public <T extends DocumentSegmentAnnotation<?>> T getAnnotationAtChar(
			Class<T> key, int loc) {
		if (loc >= 0 && loc < text.length() && this.hasAnnotation(key)) {
			List<T> s = this.getAnnotationsFor(key);
			for (T anno : s) {
				if (anno.getStart() <= loc && anno.stop > loc)
					return anno;
			}
		}
		return null;
	}
	
	public List<String> getTokenStrings(){
		List<String> result  = new ArrayList<String>();
		if(this.hasAnnotation(Token.class)){
			for(Token token: this.getAnnotationsFor(Token.class)){
				result.add(token.getTextMatched());
			}
		}
		return result;
	}
	
	public List<String> getSentanceStrings(){
		List<String> result  = new ArrayList<String>();
		if(this.hasAnnotation(Sentence.class)){
			for(Sentence sentence: this.getAnnotationsFor(Sentence.class)){
				result.add(sentence.getTextMatched());
			}
		}
		return result;
	}


}
