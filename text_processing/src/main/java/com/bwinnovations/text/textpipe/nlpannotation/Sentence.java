package com.bwinnovations.text.textpipe.nlpannotation;

import java.util.ArrayList;
import java.util.List;

import com.bwinnovations.text.textpipe.AnnotationInterface;


/**
 * A sentence annotation
 * @author Laurence Willmore
 *
 */
public class Sentence extends DocumentSegmentAnnotation<AnnotationInterface<?>>{

	/**
	 * Constructor from {@link DocumentSegmentAnnotation}
	 * @param root
	 * @param start
	 * @param stop
	 */
	public Sentence(Document root, int start, int stop) {
		super(root, start, stop);
	}
	
	public List<String> getTokenStrings(){
		List<String> result  = new ArrayList<String>();
		if(this.hasAnnotation(Token.class)){
			for(Token token: this.getAnnotationsFor(Token.class)){
				result.add(token.getTextMatched());
			}
		}
		return result;
	}

}
