package com.bwinnovations.text.textpipe.nlpannotation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public abstract class DocumentAnnotation<ANNOTATE_WITH> implements DocumentAnnotationInterface<ANNOTATE_WITH>{

	protected Document root;
	protected HashMap<Class<? extends ANNOTATE_WITH>, List<ANNOTATE_WITH>> annotations;
	protected double confidence;

	public DocumentAnnotation(Document root) {
		super();
		this.root = root;
		annotations = new HashMap<Class<? extends ANNOTATE_WITH>, List<ANNOTATE_WITH>>();
	}

	@SuppressWarnings("unchecked")
	public  void addAnnotation(ANNOTATE_WITH annotation) {
		if (annotations.containsKey(annotation.getClass())) {
			annotations.get(annotation.getClass()).add(annotation);
		} else {
			ArrayList<ANNOTATE_WITH> annos = new ArrayList<ANNOTATE_WITH>();
			annos.add(annotation);
			annotations.put((Class<? extends ANNOTATE_WITH>) annotation.getClass(), annos);
		}
	}

	public  void addAllAnnotations(
			Collection<? extends ANNOTATE_WITH> annotationCollection) {
		if (annotationCollection != null && annotationCollection.size() > 0) {
			@SuppressWarnings("unchecked")
			Class<ANNOTATE_WITH> key = (Class<ANNOTATE_WITH>) annotationCollection.iterator().next().getClass();
			if (annotations.containsKey(key)) {
				annotations.get(key).addAll(annotationCollection);
			} else {
				ArrayList<ANNOTATE_WITH> annos = new ArrayList<ANNOTATE_WITH>();
				annos.addAll(annotationCollection);
				annotations.put(key, annos);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends ANNOTATE_WITH> List<T> getAnnotationsFor(
			Class<T> key) {
		if (annotations.containsKey(key)) {
			return (List<T>) annotations.get(key);
		}
		return null;
	}

	public Set<Class<? extends ANNOTATE_WITH>> getAnnotationKeyList() {
		return annotations.keySet();
	}

	
	public double getConfidence() {
		return confidence;
	}

	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}
	
	public <T extends ANNOTATE_WITH> List<T> getSubSeqence(Class<T> key, int start, int stop){
			if(this.hasAnnotation(key)){
				List<T> s = (List<T>) this.getAnnotationsFor(key); 
				if(start>=0 && stop >= start && start<s.size() && stop < s.size()){
					return s.subList(start, stop);
				}
			}
			return null;
	}
	
	public <T extends ANNOTATE_WITH> T getAnnotationAtIndex(Class<T> key, int index){
		if(this.hasAnnotation(key)){
			List<T> s = (List<T>) this.getAnnotationsFor(key);
			if(index>=0 && index<s.size()){
				return s.get(index);
			}
		}
		return null;
	}
	
	public boolean hasAnnotation(Class<? extends ANNOTATE_WITH> annotationClass){
		if(getAnnotationKeyList().contains(annotationClass))return true;
		return false;
	}

	@Override
	public Document getDocument() {
		return root;
	}	
	
	
}
