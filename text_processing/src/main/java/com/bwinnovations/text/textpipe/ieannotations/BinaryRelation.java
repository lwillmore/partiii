package com.bwinnovations.text.textpipe.ieannotations;

import java.util.List;

import com.bwinnovations.text.textpipe.AnnotationInterface;
import com.bwinnovations.text.textpipe.nlpannotation.Document;
import com.bwinnovations.text.textpipe.nlpannotation.DocumentAnnotation;
import com.bwinnovations.text.textpipe.nlpannotation.Entity;
import com.bwinnovations.text.textpipe.nlpannotation.Token;

/**
 * A binary relation annotation between two entities.
 * @author Laurence Willmore
 *
 */
public class BinaryRelation extends DocumentAnnotation<AnnotationInterface<?>>{
	
	
	private Entity entity1;
	private Entity entity2;
	private Relation relation;
	
	/**
	 * Construct with {@link Entity} annotations and the tokens of the relation
	 * @param doc
	 * @param ent1
	 * @param ent2
	 * @param relation
	 */
	public BinaryRelation(Document doc, Entity ent1, Entity ent2, Relation relation) {
		super(doc);
		this.entity1=ent1;
		this.entity2=ent2;
		this.relation = relation; 
	}

	/**
	 * Get first {@link Entity}
	 * @return
	 */
	public Entity getEntity1() {
		return entity1;
	}

	/**
	 * Get second {@link Entity}
	 * @return
	 */
	public Entity getEntity2() {
		return entity2;
	}

	/**
	 * Get the {@link Relation}
	 * @return
	 */
	public Relation getRelation(){
		return relation;
	}
	
	

}
