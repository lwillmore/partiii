package com.bwinnovations.text.textpipe;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author Laurence Willmore
 * @param <ANNOTATE_WITH> Super-type of all annotations that may annotate this annotation.
 *
 */
public interface AnnotationInterface<ANNOTATE_WITH> {
	
	/**
	 * Adds the annotation to this annotation.
	 * @param annotation
	 */
	public  void addAnnotation(ANNOTATE_WITH annotation);
	
	

	/**
	 * Adds a collection of annotations to this annotation
	 * @param annotationCollection
	 */
	public  void addAllAnnotations(
			Collection<? extends ANNOTATE_WITH> annotationCollection);
	

	/**
	 * Returns a collection of the key annotations that annotate this annotation.
	 * @param key
	 * @return
	 */
	public <T extends ANNOTATE_WITH> List<T> getAnnotationsFor(Class<T> key);
	
	

	/**
	 * Returns the classes of the annotations that have annotated this annotation.
	 * @return
	 */
	public Set<Class<? extends ANNOTATE_WITH>> getAnnotationKeyList();
	
	

	public double getConfidence();
	
	

	void setConfidence(double confidence);
	
	

	boolean hasAnnotation(Class<? extends ANNOTATE_WITH> annotationClass);
	
	

	<T extends ANNOTATE_WITH> List<T> getSubSeqence(Class<T> key, int start, int stop);
	
	

	<T extends ANNOTATE_WITH> T getAnnotationAtIndex(Class<T> key, int index);
	 
	
	
}