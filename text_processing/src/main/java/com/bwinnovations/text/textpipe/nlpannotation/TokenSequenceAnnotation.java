package com.bwinnovations.text.textpipe.nlpannotation;

import java.util.ArrayList;
import java.util.List;

public abstract class TokenSequenceAnnotation<ANNOTATE_WITH> extends DocumentSegmentAnnotation<ANNOTATE_WITH> implements TokenAnnotationInterface<ANNOTATE_WITH>{
	
	protected List<Token> tokens;
	
	public TokenSequenceAnnotation(Document root, int start, int stop) {
		super(root, start, stop);
		tokens = root.getAnnotationsInCharRangeExclusive(Token.class, start, stop);
	}

	@Override
	public List<Token> getTokens() {
		return tokens;
	}	
}
