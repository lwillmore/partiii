package com.bwinnovations.text.textpipe.nlppipe;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;

import com.bwinnovations.text.textpipe.AnnotationInterface;
import com.bwinnovations.text.textpipe.MissingRequiredAnnotationException;
import com.bwinnovations.text.textpipe.nlpannotation.Document;
import com.bwinnovations.text.textpipe.nlpannotation.Sentence;
import com.bwinnovations.text.textpipe.nlpannotation.Token;
import com.bwinnovations.text.textpipe.nlpannotation.TreeBankPOS;
import com.bwinnovations.text.textpipe.nlpannotation.TreeBankPOS.PartOfSpeech;

public class OpenNLPPOSPipe extends NLPPipeSection{
	
	public static final String POS_MODEL_PROP = "com"+File.separator+"bwinnovations"+File.separator+"text"+File.separator+"opennlp"+File.separator+"models"+File.separator+"en-pos-maxent.bin";
	POSTaggerME tagger;
	
	

	public OpenNLPPOSPipe() {
		super();
		InputStream modelIn = null;
		POSModel model = null;
		try {
			modelIn = this.getClass().getClassLoader().getResourceAsStream(POS_MODEL_PROP);
			model = new POSModel(modelIn);
		} catch (IOException e) {
			// Model loading failed, handle the error
			e.printStackTrace();
		} finally {
			if (modelIn != null) {
				try {
					modelIn.close();
				} catch (IOException e) {
				}
			}
		}
		tagger = new POSTaggerME(model);
	}

	@Override
	public void performAnnotation(Document annotation)
			throws MissingRequiredAnnotationException {
		for(Sentence sentence : annotation.getAnnotationsFor(Sentence.class)){
			List<Token> tokens = sentence.getAnnotationsFor(Token.class);
			String[] stokens = new String[tokens.size()];
			for (int i = 0; i < stokens.length; i++) {
				stokens[i]=tokens.get(i).getTextMatched();
			}
			String[] sentPos = tagger.tag(stokens);
			for(int i = 0; i < stokens.length; i++){
				String pos = sentPos[i];
				if(TreeBankPOS.PartOfSpeech.getPOSfromString(pos)==null)System.out.println("no matching pos "+pos);
				PartOfSpeech tpos = TreeBankPOS.PartOfSpeech.getPOSfromString(pos);
				TreeBankPOS posAnno = new TreeBankPOS(tokens.get(i), tpos);
				tokens.get(i).addAnnotation(posAnno);
				sentence.addAnnotation(posAnno);
				annotation.addAnnotation(posAnno);
			}
		}
		
	}

	@Override
	public void checkForRequiredAnnotations(Document annotation)
			throws MissingRequiredAnnotationException {
		List<Class<? extends AnnotationInterface>> missing = new ArrayList<Class<? extends AnnotationInterface>>();
		if (!annotation.getAnnotationKeyList().contains(Sentence.class))
			missing.add(Sentence.class);
		if (!annotation.getAnnotationKeyList().contains(Token.class))
			missing.add(Token.class);
		if (missing.size() > 0) {
			throw new MissingRequiredAnnotationException(
					"OpenNLPPOSPipe is missing required Annotations on the Document",
					missing);
		}		
	}

}
