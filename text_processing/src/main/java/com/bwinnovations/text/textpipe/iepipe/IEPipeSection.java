package com.bwinnovations.text.textpipe.iepipe;

import com.bwinnovations.text.textpipe.PipeSection;
import com.bwinnovations.text.textpipe.nlpannotation.Document;

/**
 * @author Laurence Willmore
 *
 */
public abstract class IEPipeSection extends PipeSection<Document>{

}
