package com.bwinnovations.text.textpipe.nlppipe;

import com.bwinnovations.text.textpipe.MissingRequiredAnnotationException;
import com.bwinnovations.text.textpipe.nlpannotation.Document;
import com.bwinnovations.text.textpipe.nlpannotation.Token;
import com.bwinnovations.text.textpipe.nlpannotation.WordnetSense;
import com.bwinnovations.text.wordnet.DictionaryDimensions;
import com.bwinnovations.text.wordnet.WordnetUtils;

public class PipePlayGround {

	public static void main(String[] args) {
		OpenNLPSentencePipe sentPipe = new OpenNLPSentencePipe();
		OpenNLPTokenPipe tokPipe = new OpenNLPTokenPipe();
		OpenNLPPOSPipe posPipe = new OpenNLPPOSPipe();
		LeskWordSensePipe wsPipe = new LeskWordSensePipe(new DictionaryDimensions(WordnetUtils.loadDictionary(args[0])));
		Document d = new Document(
				"This is just a test to see how well the word sense disambiguation works. The bass player in the band wore a large crown.");
		try {
			sentPipe.annotate(d);
			tokPipe.annotate(d);
			posPipe.annotate(d);
			wsPipe.annotate(d);
		} catch (MissingRequiredAnnotationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(Token token:d.getAnnotationsFor(Token.class)){
			if(token.hasAnnotation(WordnetSense.class)){
				System.out.println(token.getTextMatched()+" : "+token.getAnnotationsFor(WordnetSense.class).get(0).getSynset().getGloss());
			}
		}
	}

}
