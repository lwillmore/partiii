package com.bwinnovations.text.textpipe.nlpannotation;

import com.bwinnovations.text.textpipe.AbstractAnnotation;
import com.bwinnovations.text.textpipe.AnnotationInterface;

/**
 * A Named Entity annotation
 * @author Laurence Willmore
 *
 */
public class Entity extends TokenSequenceAnnotation<AnnotationInterface<?>>{

	public Entity(Document root, int start, int stop) {
		super(root, start, stop);
	}
}
