package com.bwinnovations.text.textpipe.nlpannotation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.bwinnovations.text.textpipe.AnnotationInterface;

public abstract class TokenCompoundAnnotation<ANNOTATE_WITH> extends DocumentCompoundAnnotation<ANNOTATE_WITH,Token> implements TokenAnnotationInterface<ANNOTATE_WITH>{

	public TokenCompoundAnnotation(Document doc, List<Token> tokens) {
		super(doc);
		for(Token t:tokens){
			this.addSubAnnotation(t);
		}
	}

	@Override
	public List<Token> getTokens() {
		return this.getSubAnnotationsFor(Token.class);
	}
	
	
	public String getTokensAsString(){
		return StringUtils.join(this.getTokensAsStringList(), " ");
	}
	
	public List<String> getTokensAsStringList(){
		ArrayList<String> res = new ArrayList<String>();
		for(Token t : this.getTokens()){
			res.add(t.getTextMatched());
		}
		return res;
	}
	

}
