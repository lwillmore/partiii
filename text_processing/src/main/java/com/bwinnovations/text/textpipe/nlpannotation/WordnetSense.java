package com.bwinnovations.text.textpipe.nlpannotation;

import com.bwinnovations.text.textpipe.AnnotationInterface;

import edu.mit.jwi.item.ISynset;

/**
 * An {@link TokenAnnotationInterface} for the Wordnet synset of that token.
 * @author Laurence Willmore
 *
 */
public class WordnetSense extends TokenSingletonAnnotation<AnnotationInterface<?>>{
	
	private ISynset synset;

	/**
	 * Default constructor, takes the token and the {@link ISynset}.
	 * @param token
	 * @param synset
	 */
	public WordnetSense(Token token, ISynset synset) {
		super(token);
		this.synset=synset;
	}
	
	/**
	 * Getter for the {@link ISynset}.
	 * @return
	 */
	public ISynset getSynset(){
		return synset;
	}

}
