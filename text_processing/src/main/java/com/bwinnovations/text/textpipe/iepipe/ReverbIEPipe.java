package com.bwinnovations.text.textpipe.iepipe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.bwinnovations.text.textpipe.AnnotationInterface;
import com.bwinnovations.text.textpipe.MissingRequiredAnnotationException;
import com.bwinnovations.text.textpipe.ieannotations.BinaryRelation;
import com.bwinnovations.text.textpipe.ieannotations.Relation;
import com.bwinnovations.text.textpipe.nlpannotation.Document;
import com.bwinnovations.text.textpipe.nlpannotation.Entity;
import com.bwinnovations.text.textpipe.nlpannotation.Sentence;
import com.bwinnovations.text.textpipe.nlpannotation.Token;

import edu.washington.cs.knowitall.extractor.ReVerbExtractor;
import edu.washington.cs.knowitall.extractor.conf.ConfidenceFunction;
import edu.washington.cs.knowitall.extractor.conf.ReVerbConfFunction;
import edu.washington.cs.knowitall.nlp.ChunkedSentence;
import edu.washington.cs.knowitall.nlp.OpenNlpSentenceChunker;
import edu.washington.cs.knowitall.nlp.extraction.ChunkedArgumentExtraction;
import edu.washington.cs.knowitall.nlp.extraction.ChunkedBinaryExtraction;
import edu.washington.cs.knowitall.nlp.extraction.ChunkedExtraction;

/**
 * 
 * @author Laurence Willmore
 * 
 */
public class ReverbIEPipe extends IEPipeSection {
	private OpenNlpSentenceChunker chunker;
	private ReVerbExtractor reverb;
	private ConfidenceFunction confFunc;

	public ReverbIEPipe() throws IOException {
		chunker = new OpenNlpSentenceChunker();
		reverb = new ReVerbExtractor();
		confFunc = new ReVerbConfFunction();
	}

	@Override
	public void performAnnotation(Document annotation)
			throws MissingRequiredAnnotationException {
		for (Sentence sentence : annotation.getAnnotationsFor(Sentence.class)) {
			ChunkedSentence sent = chunker.chunkSentence(sentence
					.getTextMatched());
			for (ChunkedBinaryExtraction extr : reverb.extract(sent)) {
				double conf = confFunc.getConf(extr);
				
				Entity ent1 = getEntityFrom(annotation,extr.getArgument1(),
						sentence);

				Entity ent2 = getEntityFrom(annotation,extr.getArgument2(),
						sentence);
				
				Relation relation = getRelationFrom(annotation, extr.getRelation(), sentence);
				
				

				BinaryRelation br = new BinaryRelation(annotation, ent1, ent2,
						relation);
				br.setConfidence(conf);
				annotation.addAnnotation(br);
			}
		}

	}

	private Relation getRelationFrom(Document annotation,
			ChunkedExtraction relation, Sentence sentence) {
		int firstTokI = relation.getStart();
		int lastTokI = firstTokI + relation.getLength();
		List<Token> tokens = sentence.getAnnotationsFor(Token.class).subList(firstTokI, lastTokI);
		Relation r= new Relation(annotation, tokens);
		return r;
	}

	private Entity getEntityFrom(Document root,ChunkedArgumentExtraction ent,
			Sentence sentence) {
		int firstTokI = ent.getStart();
		int lastTokI = firstTokI + ent.getLength() - 1;
		Token s = sentence.getAnnotationAtIndex(Token.class, firstTokI);
		Token e = sentence.getAnnotationAtIndex(Token.class, lastTokI);
		int a1sc = s.getStart();
		int a1ec = e.getStop();
		return new Entity(root, a1sc, a1ec);
	}

	@Override
	public void checkForRequiredAnnotations(Document annotation)
			throws MissingRequiredAnnotationException {
		List<Class<? extends AnnotationInterface>> missing = new ArrayList<Class<? extends AnnotationInterface>>();
		if (!annotation.getAnnotationKeyList().contains(Sentence.class))
			missing.add(Sentence.class);
		if (!annotation.getAnnotationKeyList().contains(Token.class))
			missing.add(Token.class);
		if (missing.size() > 0) {
			throw new MissingRequiredAnnotationException(
					"ReverbIEPipe is missing required Annotations on the Document",
					missing);
		}
	}
}
