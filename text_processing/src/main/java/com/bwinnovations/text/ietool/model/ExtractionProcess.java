package com.bwinnovations.text.ietool.model;

/**
 * This is the object that holds a complete Information Extraction pipeline from source to action.
 * @author Laurence Willmore
 *
 */
public abstract class ExtractionProcess {

}
