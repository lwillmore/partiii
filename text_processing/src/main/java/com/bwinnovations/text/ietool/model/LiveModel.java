package com.bwinnovations.text.ietool.model;

/**
 * Sub model for manipulation of Extraction pipelines.
 * @author Laurence Willmore
 *
 */
public class LiveModel {
	
	private ProcessRack pRack;

	/**
	 * Constructor for initialising with an existing or loaded rack.
	 * @param pRack
	 */
	public LiveModel(ProcessRack pRack) {
		super();
		this.pRack = pRack;
	}

	/**
	 * Default constructor with empty rack.
	 */
	public LiveModel() {
		super();
		this.pRack= new ProcessRack();
	}
	
	public void addNewProcess(ExtractionProcess newP){
		pRack.addNewProcess(newP);
	}
	
	

}
