package com.bwinnovations.text.ietool.model;

/**
 * This is an {@link ExtractionProcess} that uses the Washington University
 * Reverb software to perform the Information Extraction. See below for their licensing.
 * 
 * 
 * 
 * @author Laurence Willmore
 * 
 */
public class ReverbExractionProcess extends ExtractionProcess{

}
