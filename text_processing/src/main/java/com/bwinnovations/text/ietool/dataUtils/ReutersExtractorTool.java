package com.bwinnovations.text.ietool.dataUtils;

import java.io.File;

import org.apache.lucene.benchmark.utils.ExtractReuters;

/**
 * This is a wrapper for org.apache.lucene.benchmark.utils.ExtractReuters
 * 
 * It is used to extract Reuters SGML format files into text files.
 * 
 * @author Laurence Willmore
 *
 */
public class ReutersExtractorTool {
	
	/**
	 * Simple main to make use of the Apache ExtractReuters class.
	 * @param args 1st - path to Reuters directory. 2nd - path to output directory.
	 */
	public static void main(String[] args) {
		File in = new File(args[0]);
		File out = new File(args[1]);
		ExtractReuters myExtractor = new ExtractReuters(in, out);
		myExtractor.extract();
	}

}
