package com.bwinnovations.text.ietool.model;

/**
 * Overarching model object for entire system.
 * Consists of a  {@link LiveModel} and a {@link TestModel}.
 * @author Laurence Willmore
 *
 */
public class SystemModel {
	
	private LiveModel liveM;
	private TestModel testM;
	
	/**
	 * Default empty constructor.
	 */
	public SystemModel(){
		this.liveM = new LiveModel();
		this.testM = new TestModel();
	}

	@SuppressWarnings("javadoc")
	public LiveModel getLiveM() {
		return liveM;
	}	

	@SuppressWarnings("javadoc")
	public TestModel getTestM() {
		return testM;
	}		

}
