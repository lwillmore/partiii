package com.bwinnovations.text.ietool.model;

import java.util.ArrayList;

/**
 * Object that holds and manipulates the ExtractionProcess data structure.
 * This will be the Object that is saved and loaded.
 * @author Laurence
 *
 */
public class ProcessRack {
	
	private ArrayList<ExtractionProcess> procs;
	
	/**
	 * Default contstructor
	 */
	public ProcessRack(){
		this.procs = new ArrayList<ExtractionProcess>();
	}

	/**
	 * Adds a new {@link ExtractionProcess} to this {@link ProcessRack}
	 * @param newP {@link ExtractionProcess}
	 */
	public void addNewProcess(ExtractionProcess newP) {
		procs.add(newP);
	}
	
	
	
	

}
