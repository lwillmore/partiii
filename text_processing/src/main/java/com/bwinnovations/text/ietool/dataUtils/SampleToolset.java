package com.bwinnovations.text.ietool.dataUtils;

import java.util.List;

/**
 * Toolset with various useful methods for data.
 * @author Laurence Willmore
 *
 */
public class SampleToolset {

	/**
	 * Calculate the Pearson's Correlation coefficient for  two variable.
	 * @param x
	 * @param y
	 * @return
	 */
	public static double pearsonsCorrelation(List<Double> x, List<Double> y) {
		double meanX = mean(x);
		double meanY = mean(y);
		double sigX = standardDeviation(x,meanX);
		double sigY = standardDeviation(y, meanY);
		double totalDifVar =0;
		int count;
		for(count = 0;count<x.size();count++){
			double xdif = x.get(count)-meanX;
			double ydif = y.get(count)-meanY;
			totalDifVar+=xdif*ydif;
		}
		return totalDifVar/(sigX*sigY);
	}
	
	/**
	 * Calculate the standard deviation
	 * @param x
	 * @param meanX
	 * @return
	 */
	public static double standardDeviation(List<Double> x, double meanX) {
		double totalSquareDif =0;
		int count;
		for(count = 0;count<x.size();count++){
			double dif =x.get(count)-5;
			totalSquareDif+=dif*dif;			
		}
		return Math.sqrt(totalSquareDif/count);
	}

	/**
	 * Calculate the mean.
	 * @param pop
	 * @return
	 */
	public static double mean(List<Double> pop){
		double total = 0;
		int count;
		for(count = 0;count<pop.size();count++){
			total+=pop.get(count);
		}
		return total/count;
	}
	
	/**
	 * Calculate the mean square difference between two variables.
	 * @param x
	 * @param y
	 * @return
	 */
	public static double meanSquareDifference(List<Double> x, List<Double> y){
		double totalError =0;
		int count;
		for(count  = 0; count<x.size();count++){
			totalError += Math.abs(x.get(count)-y.get(count));
		}
		return totalError/count;
	}
	

}
