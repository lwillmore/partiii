package com.bwinnovations.text.ietool.dataUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.bwinnovations.text.textpipe.MissingRequiredAnnotationException;
import com.bwinnovations.text.textpipe.ieannotations.BinaryRelation;
import com.bwinnovations.text.textpipe.iepipe.ReverbIEPipe;
import com.bwinnovations.text.textpipe.nlpannotation.Document;
import com.bwinnovations.text.textpipe.nlpannotation.Sentence;
import com.bwinnovations.text.textpipe.nlppipe.OpenNLPPOSPipe;
import com.bwinnovations.text.textpipe.nlppipe.OpenNLPSentencePipe;
import com.bwinnovations.text.textpipe.nlppipe.OpenNLPTokenPipe;
import com.bwinnovations.fileutils.FileUtils;

/**
 * This class is for extracting the relations discovered by the
 * {@link ReverbIEPipe} from a corpus of text.
 * 
 * It is also doing a lot of counting of features of the relations so that an
 * analysis can be made to determine a suitable method for comparison.
 * 
 * Aiming to reduce to important features:
 * - (significant noun and verb) | (significant noun or verb)
 * - negation
 * - preposition
 * 
 * Remove things such as quantifiers
 * 
 * I want:
 * 
 * 
 * @author Laurence Willmore
 * 
 */
public class ReverbRelationExtractor {

	private File corpusDirectory;
	private File extractionDirectory;
	private ReverbIEPipe revpipe;
	private OpenNLPSentencePipe sentpipe;
	private OpenNLPTokenPipe tokPipe;
	private OpenNLPPOSPipe posPipe;

	private int docCount;
	private int relationCount;

	public static void main(String[] args) {
		File corpusDirectory = new File(args[0]);
		File extractionDirectory = new File(args[1]);
		ReverbRelationExtractor myExtractor = new ReverbRelationExtractor(
				corpusDirectory, extractionDirectory);
		myExtractor.extract();
	}

	public ReverbRelationExtractor(File corpusDirectory,
			File extractionDirectory) {
		this.corpusDirectory = corpusDirectory;
		this.extractionDirectory = extractionDirectory;
		try {
			this.revpipe = new ReverbIEPipe();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.sentpipe = new OpenNLPSentencePipe();
		this.tokPipe = new OpenNLPTokenPipe();
		this.posPipe = new OpenNLPPOSPipe();
	}

	private void extract() {
		docCount = 0;
		relationCount = 0;
		for (File inFile : corpusDirectory.listFiles()) {
			if (isValid(inFile)) {
				docCount++;
				try {
					extractRelationsFrom(FileUtils.getStringFrom(inFile),
							inFile.getName());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("Documents: " + docCount + "\nRelations: "
				+ relationCount);
	}

	private void extractRelationsFrom(String rawString, String sourceName) {
		Document doc = new Document(rawString);
		try {
			sentpipe.annotate(doc);
			tokPipe.annotate(doc);
			posPipe.annotate(doc);
			revpipe.annotate(doc);
			List<BinaryRelation> relations = doc
					.getAnnotationsFor(BinaryRelation.class);
			if (relations != null && relations.size() > 0) {
				File out = new File(extractionDirectory.getAbsolutePath()
						+ File.separator + sourceName + "_relations.txt");
				try {
					out.createNewFile();
					try {
						// Create file
						FileWriter fstream = new FileWriter(out);
						BufferedWriter outwriter = new BufferedWriter(fstream);
						for (BinaryRelation rel : relations) {
							relationCount++;
							Sentence parentSentence = doc.getAnnotationAtChar(Sentence.class, rel.getEntity1().getStart());
							outwriter.append(parentSentence.getTextMatched() + "\n");
							String relation = rel.getRelation()
									.getTokensAsString();
							outwriter.append(relation + "\n");
						}
						// Close the output stream
						outwriter.close();
					} catch (Exception e) {// Catch exception if any
						System.err.println("Error: " + e.getMessage());
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (MissingRequiredAnnotationException e1) {
			e1.printStackTrace();
		}

	}

	

	private boolean isValid(File inFile) {
		return inFile.isFile();
	}

}
