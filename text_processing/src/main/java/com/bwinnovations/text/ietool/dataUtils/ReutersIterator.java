package com.bwinnovations.text.ietool.dataUtils;

import java.io.File;
import java.io.IOException;

import com.bwinnovations.fileutils.FileUtils;
import com.bwinnovations.text.textpipe.nlpannotation.Document;

/**
 * Class for iterating over the Reuters text document directory.
 * @author Laurence Willmore
 *
 */
public class ReutersIterator extends CorpusIterator{
	
	
	private File[] fileList;
	int currentIndex;
	int length;

	/**
	 * Default constructor that takes the directory of the corpus text files as an input
	 * @param corpusDirectoryPath
	 */
	public ReutersIterator(String corpusDirectoryPath){
		super();
		File corpusDirectory=new File(corpusDirectoryPath);
		fileList = corpusDirectory.listFiles();
		currentIndex=0;
		length = fileList.length;
	}

	@Override
	public boolean hasNext() {
		if(length>0 && currentIndex<length)return true;
		return false;
	}

	@Override
	public Document next() {
		
		File next = fileList[currentIndex];
		Document d = null;
		System.out.println(next.getAbsolutePath());
		try {
			d = new Document(FileUtils.getStringFrom(next));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		currentIndex++;
		return d;
	}

	@Override
	public void remove() {
		currentIndex++;
	}
}
