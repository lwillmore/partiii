package com.bwinnovations.text.experiments;

import java.util.ArrayList;
import java.util.List;

import com.bwinnovations.fileutils.FileLineIterator;
import com.bwinnovations.text.ietool.dataUtils.SampleToolset;
import com.bwinnovations.text.wordnet.DictionaryDimensions;
import com.bwinnovations.text.wordnet.ResnikDistanceFactory;
import com.bwinnovations.text.wordnet.ResnikDistanceFactory.ResnikDistance;
import com.bwinnovations.text.wordnet.WordnetUtils;

/**
 * Experiment to test word level semantic metrics.
 * 
 * @author Laurence Willmore
 * 
 */
public class Experiment1 {

	private static int W1 = 0, W2 = 1, HM = 2;
	private ArrayList<String> words1, words2;
	private ArrayList<Double> hMean, resnik;

	public void run(String probPath, String dictionaryPath, String datapath) {
		// initialize arrays
		words1 = new ArrayList<String>();
		words2 = new ArrayList<String>();
		hMean = new ArrayList<Double>();
		resnik = new ArrayList<Double>();
		// initialize Resnik
		DictionaryDimensions dd = new DictionaryDimensions(
				WordnetUtils.loadDictionary(dictionaryPath));
		ResnikDistance rd = ResnikDistanceFactory.loadFromResnikProbabilities(
				probPath, dd);
		// Open data
		FileLineIterator iterator = new FileLineIterator(datapath);
		iterator.next();
		// for each entry calculate resnik
		while (iterator.hasNext()) {
			String line = iterator.next();
			String[] values = line.split("\t");
			String w1 = values[W1].trim();
			String w2 = values[W2].trim();
			double normHMean = Double.parseDouble(values[HM].trim());
			double resScore = rd.nouns(w1, w2);
			if (dd.wordnetContains(w1) && dd.wordnetContains(w2)) {
				words1.add(w1);
				words2.add(w2);
				hMean.add(normHMean);
				resnik.add(resScore);
			}
		}
		System.out.println(SampleToolset.pearsonsCorrelation(resnik, hMean));
	}

}
