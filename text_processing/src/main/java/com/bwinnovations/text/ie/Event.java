package com.bwinnovations.text.ie;

import com.bwinnovations.text.textpipe.AbstractAnnotation;

/**
 * Basic abstract class for all Information Extraction events.
 * @author Laurence Willmore
 *
 */
public abstract class Event{

}
