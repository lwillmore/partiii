package com.bwinnovations.text.wordnet;

import com.bwinnovations.text.textpipe.nlpannotation.Token;

import edu.mit.jwi.item.ISynsetID;

/**
 * Interface for any pairwise distance metric
 * @author Laurence Willmore
 *
 */
public interface PairWiseDistance {
	
	/**
	 * Get distance between nouns
	 * Will assume the most common synset.
	 * @param noun1
	 * @param noun2
	 * @return
	 */
	double nouns(String noun1, String noun2);
	
	/**
	 * Get distance between verbs
	 * Will assume the most common synset.
	 * @param verb1
	 * @param verb2
	 * @return
	 */
	double verbs(String verb1, String verb2);
	
	/**
	 * Get distance between noun Synset
	 * @param noun1
	 * @param noun2
	 * @return
	 */
	double nouns(ISynsetID noun1, ISynsetID noun2);
	
	/**
	 * Get distance between verb Synset
	 * @param verb1
	 * @param verb2
	 * @return
	 */
	double verbs(ISynsetID verb1, ISynsetID verb2);
	
	

}
