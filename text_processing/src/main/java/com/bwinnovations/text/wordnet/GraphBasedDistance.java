package com.bwinnovations.text.wordnet;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.mit.jwi.item.IPointer;
import edu.mit.jwi.item.ISynsetID;
import edu.mit.jwi.item.Pointer;

public abstract class GraphBasedDistance implements PairWiseDistance{
	
	protected DictionaryDimensions dd;
	
	public GraphBasedDistance(DictionaryDimensions dd){
		super();
		this.dd = dd;
	}
	
	
	
	protected void getPathToRoot(List<List<ISynsetID>> pathlists, List<ISynsetID> workingpath) {
		Map<IPointer, List<ISynsetID>> rmap = dd.dict.getSynset(workingpath.get(workingpath.size()-1)).getRelatedMap();
		for(IPointer pointer : rmap.keySet()){
			if(pointer.getName().equals(Pointer.HYPERNYM.getName())){
				List<ISynsetID> hypernyms = rmap.get(pointer);
				if(hypernyms.size()>1){					
					for(int i=1;i<hypernyms.size();i++){
						List<ISynsetID> branch = new ArrayList<ISynsetID>();
						branch.addAll(workingpath);
						branch.add(hypernyms.get(i));
						pathlists.add(branch);
						getPathToRoot(pathlists,branch);
					}
					workingpath.add(hypernyms.get(0));
					getPathToRoot(pathlists,workingpath);
				}
				else{
					workingpath.add(hypernyms.get(0));
					getPathToRoot(pathlists,workingpath);
				}						
			}
		}	
	}

}
