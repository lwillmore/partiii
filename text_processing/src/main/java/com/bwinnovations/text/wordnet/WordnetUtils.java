package com.bwinnovations.text.wordnet;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;

public class WordnetUtils {
	
	public static IDictionary loadDictionary(String dictionaryPath){
		URL url = null; 
		try {
			url = new URL("file",null,dictionaryPath);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		IDictionary dict = new Dictionary(url);
		try {
			dict.open();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dict;
	}

}
