package com.bwinnovations.text.wordnet;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.bwinnovations.text.ietool.dataUtils.CorpusIterator;
import com.bwinnovations.text.ietool.dataUtils.ReutersIterator;
import com.bwinnovations.text.textpipe.MissingRequiredAnnotationException;
import com.bwinnovations.text.textpipe.nlpannotation.Document;
import com.bwinnovations.text.textpipe.nlpannotation.Sentence;
import com.bwinnovations.text.textpipe.nlpannotation.Token;
import com.bwinnovations.text.textpipe.nlpannotation.TreeBankPOS;
import com.bwinnovations.text.textpipe.nlpannotation.WordnetSense;
import com.bwinnovations.text.textpipe.nlppipe.LeskWordSensePipe;
import com.bwinnovations.text.textpipe.nlppipe.OpenNLPPOSPipe;
import com.bwinnovations.text.textpipe.nlppipe.OpenNLPSentencePipe;
import com.bwinnovations.text.textpipe.nlppipe.OpenNLPTokenPipe;
import com.bwinnovations.generalutils.Counter;
import com.bwinnovations.generalutils.PairContainer;

import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.IPointer;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.ISynsetID;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.item.Pointer;
import edu.mit.jwi.item.SynsetID;

/**
 * Factory for instantiating ResnikDistance
 * 
 * @author Laurence Willmore
 * 
 */
@SuppressWarnings("rawtypes")
public class ResnikDistanceFactory {

	/**
	 * Takes a {@link CorpusIterator} to build a {@link ResnikDistance}.
	 * 
	 * @param corpusIterator
	 * @return
	 */
	public static <T extends CorpusIterator> ResnikDistance buildFromCorpus(
			T corpusIterator, DictionaryDimensions dd) {
		OpenNLPSentencePipe sentPipe = new OpenNLPSentencePipe();
		OpenNLPTokenPipe tokPipe = new OpenNLPTokenPipe();
		OpenNLPPOSPipe posPipe = new OpenNLPPOSPipe();
		WordSenseCounter wsPipe = new WordSenseCounter(dd);
		while (corpusIterator.hasNext()) {
			Document d = corpusIterator.next();
			try {
				sentPipe.annotate(d);
				tokPipe.annotate(d);
				posPipe.annotate(d);
				wsPipe.annotate(d);
			} catch (MissingRequiredAnnotationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return new ResnikDistance(wsPipe.resolveCounts(), dd);
	}

	public static <T extends CorpusIterator> ResnikDistance saveResnikProbabilities(
			T corpusIterator, DictionaryDimensions dd, String path) {
		ResnikDistance rd = buildFromCorpus(corpusIterator, dd);
		Map<MSynsetID, Double> toSave = new HashMap<MSynsetID, Double>();
		for (ISynsetID id : rd.synsProbs.keySet()) {
			toSave.put(new MSynsetID(id), rd.synsProbs.get(id));
		}
		try {
			FileOutputStream fileOut = new FileOutputStream(path);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(toSave);
			out.close();
			fileOut.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
		return rd;
	}

	public static ResnikDistance loadFromResnikProbabilities(String filepath,
			DictionaryDimensions dd) {
		Map<MSynsetID, Double> loaded = null;
		Map<ISynsetID, Double> converted = new HashMap<ISynsetID, Double>();
		try {
			FileInputStream fileIn = new FileInputStream(filepath);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			loaded = (Map<MSynsetID, Double>) in.readObject();
			in.close();
			fileIn.close();
		} catch (IOException i) {
			i.printStackTrace();
		} catch (ClassNotFoundException c) {
			c.printStackTrace();
		}
		for (MSynsetID id : loaded.keySet()) {
			converted.put(id.getOriginal(), loaded.get(id));
		}
		return new ResnikDistance(converted, dd);
	}

	/**
	 * Pairwise distance metric based on Resnik's methods
	 * 
	 * @author Laurence Willmore
	 * 
	 */
	public static class ResnikDistance extends GraphBasedDistance {

		private Map<ISynsetID, Double> synsProbs;

		private ResnikDistance(Map<ISynsetID, Double> synsProbs,
				DictionaryDimensions dd) {
			super(dd);
			this.synsProbs = synsProbs;
		}

		@Override
		public double nouns(String noun1, String noun2) {
			// Get the wordnet words
			List<String> lemmas1 = dd.stemmer.findStems(noun1, POS.NOUN);
			List<String> lemmas2 = dd.stemmer.findStems(noun2, POS.NOUN);
			IIndexWord wnnoun1 = dd.dict.getIndexWord(lemmas1.get(0), POS.NOUN);
			IIndexWord wnnoun2 = dd.dict.getIndexWord(lemmas2.get(0), POS.NOUN);

			// Get all the paths to the root for 1
			IWordID wordID = wnnoun1.getWordIDs().get(0);
			IWord word = dd.dict.getWord(wordID);
			ISynsetID synid = word.getSynset().getID();
			List<List<ISynsetID>> paths1 = new ArrayList<List<ISynsetID>>();
			List<ISynsetID> internal1 = new ArrayList<ISynsetID>();
			internal1.add(synid);
			paths1.add(internal1);
			getPathToRoot(paths1, internal1);

			// Get all the paths to the root for 2
			IWordID wordID2 = wnnoun2.getWordIDs().get(0);
			IWord word2 = dd.dict.getWord(wordID2);
			ISynsetID synid2 = word2.getSynset().getID();
			List<List<ISynsetID>> paths2 = new ArrayList<List<ISynsetID>>();
			List<ISynsetID> internal2 = new ArrayList<ISynsetID>();
			internal2.add(synid2);
			paths2.add(internal2);
			getPathToRoot(paths2, internal2);

			// Find the subsumer with the lowest amount of edges
			double lowestProb = 1.0;
			ISynsetID subsumer = dd.NOUN_HYP_ROOT.getID();
			for (List<ISynsetID> path : paths1) {
				for (List<ISynsetID> path2 : paths2) {
					ISynsetID id = getSubsumerProb(path ,path2);
					double prob = synsProbs.get(id);
					if(prob<lowestProb){
						lowestProb = prob;
						subsumer = id;
					}
				}
			}
			return -java.lang.Math.log(lowestProb);
		}

		private ISynsetID getSubsumerProb(
				List<ISynsetID> path1, List<ISynsetID> path2) {
			int i1 = path1.size()-1;
			int i2 = path2.size()-1;
			while(i1>=0 && i2 >=0){
				ISynset s1 = dd.dict.getSynset(path1.get(i1--));
				ISynset s2 = dd.dict.getSynset(path2.get(i2--));
				if(s1.hashCode()==s2.hashCode())return s1.getID();
			}
			return null;
		}

		@Override
		public double verbs(String verb1, String verb2) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public double nouns(ISynsetID noun1, ISynsetID noun2) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public double verbs(ISynsetID verb1, ISynsetID verb2) {
			// TODO Auto-generated method stub
			return 0;
		}

	}

	/**
	 * Class that extends LeskWordSensePipe so that it will count all the
	 * wordsenses it sees.
	 * 
	 * @author Laurence Willmore
	 * 
	 */
	public static class WordSenseCounter extends LeskWordSensePipe {

		private HashMap<ISynsetID, Counter> synsCounts;
		private HashMap<ISynsetID, Double> synsProbs;

		/**
		 * Defualt Constructor
		 * 
		 * @param dd
		 */
		public WordSenseCounter(DictionaryDimensions dd) {
			super(dd);
			synsCounts = new HashMap<ISynsetID, Counter>();
			synsProbs = new HashMap<ISynsetID, Double>();
		}

		@Override
		protected void annotate(Token token, Sentence sentence) {
			POS pos = token.getAnnotationsFor(TreeBankPOS.class).get(0)
					.getPos().toWordnetPOS();
			String tokString = token.getTextMatched();
			if (pos != null) {
				List<String> stems = dd.stemmer.findStems(
						token.getTextMatched(), pos);
				if (!stems.isEmpty()) {
					String stem = stems.get(0);
					IIndexWord word = dd.dict.getIndexWord(stem, pos);
					int index = 1;
					while (index < stems.size() && word == null) {
						stem = stems.get(index);
						word = dd.dict.getIndexWord(stem, pos);
						index++;
					}
					if (word != null) {
						List<String> context = getStemmedStringList(sentence
								.getAnnotationsFor(Token.class));
						while (context.contains(stems.get(0)))
							context.remove(stems.get(0));
						/**
						 * This is the Lesk algorithm
						 */
						List<ISynset> possibleSynsets = dd
								.getPossibleSynsets(word);
						ISynset bestSense = possibleSynsets.get(0);
						int maxOverlap = 0;
						for (ISynset sense : possibleSynsets) {
							List<String> signature = getStemmedStringList(getSignatureWords(sense));
							while (signature.contains(stems.get(0)))
								signature.remove(stems.get(0));
							int overlap = computeOverlap(signature, context);
							if (overlap > maxOverlap) {
								bestSense = sense;
								maxOverlap = overlap;
							}
						}
						token.addAnnotation(new WordnetSense(token, bestSense));
						incrementSense(bestSense);
					}
				}
			}
		}

		private void incrementSense(ISynset sense) {
			ISynsetID id = sense.getID();
			if (synsCounts.containsKey(id)) {
				synsCounts.get(id).inc();
			} else
				synsCounts.put(id, new Counter(1));
		}

		/**
		 * Call this once all counting is complete. It returns a hashmap of
		 * probabilitis for the synsets.
		 */
		public Map<ISynsetID, Double> resolveCounts() {
			filterCounts();
			return calculateProbs();
		}

		private Map<ISynsetID, Double> calculateProbs() {
			Map<IPointer, List<ISynsetID>> rmap = dd.NOUN_HYP_ROOT
					.getRelatedMap();
			double rootCount = synsCounts.get(dd.NOUN_HYP_ROOT.getID())
					.getCount();
			synsProbs.put(dd.NOUN_HYP_ROOT.getID(), 1.0);
			for (IPointer pointer : rmap.keySet()) {
				if (pointer.getName().equals(Pointer.HYPONYM.getName())) {
					List<ISynsetID> hyponyms = rmap.get(pointer);
					for (ISynsetID synID : hyponyms) {
						probHelper(synID, rootCount);
					}
				}
			}
			return synsProbs;
		}

		private void probHelper(ISynsetID synsID, double rootCount) {
			ISynset syn = dd.dict.getSynset(synsID);
			Map<IPointer, List<ISynsetID>> rmap = syn.getRelatedMap();
			synsProbs.put(synsID, ((double) synsCounts.get(synsID).getCount())
					/ rootCount);
			for (IPointer pointer : rmap.keySet()) {
				if (pointer.getName().equals(Pointer.HYPONYM.getName())) {
					List<ISynsetID> hyponyms = rmap.get(pointer);
					for (ISynsetID synID : hyponyms) {
						probHelper(synID, rootCount);
					}
				}
			}

		}

		private void filterCounts() {
			Map<IPointer, List<ISynsetID>> rmap = dd.NOUN_HYP_ROOT
					.getRelatedMap();
			int rootCount = 0;
			for (IPointer pointer : rmap.keySet()) {
				if (pointer.getName().equals(Pointer.HYPONYM.getName())) {
					List<ISynsetID> hyponyms = rmap.get(pointer);
					for (ISynsetID synID : hyponyms) {
						rootCount += filterHelper(synID);
					}
				}
			}
			if (synsCounts.containsKey(dd.NOUN_HYP_ROOT.getID())) {
				synsCounts.get(dd.NOUN_HYP_ROOT.getID()).add(rootCount);
			} else {
				synsCounts
						.put(dd.NOUN_HYP_ROOT.getID(), new Counter(rootCount));
			}
		}

		private int filterHelper(ISynsetID synsID) {
			ISynset syn = dd.dict.getSynset(synsID);
			Map<IPointer, List<ISynsetID>> rmap = syn.getRelatedMap();
			int count = 0;
			for (IPointer pointer : rmap.keySet()) {
				if (pointer.getName().equals(Pointer.HYPONYM.getName())) {
					List<ISynsetID> hyponyms = rmap.get(pointer);
					for (ISynsetID synID : hyponyms) {
						count += filterHelper(synID);
					}
				}
			}
			if (synsCounts.containsKey(synsID)) {
				synsCounts.get(synsID).add(count);
				return synsCounts.get(synsID).getCount();
			} else {
				if (count == 0)
					count++;
				synsCounts.put(synsID, new Counter(count));
				return count;
			}
		}

	}

	/**
	 * A serialised wrapper for {@link ISynsetID}
	 * 
	 * @author Laurence Willmore
	 * 
	 */
	private static class MSynsetID implements ISynsetID, Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		int offset;
		String posName;

		public MSynsetID(ISynsetID in) {
			offset = in.getOffset();
			posName = in.getPOS().name();
		}

		@Override
		public POS getPOS() {
			return POS.valueOf(posName);
		}

		@Override
		public int getOffset() {
			return offset;
		}

		public SynsetID getOriginal() {
			return new SynsetID(offset, POS.valueOf(posName));
		}

	}

	public static void main(String[] args) {
		ResnikDistance rd1 = ResnikDistanceFactory.saveResnikProbabilities(
				new ReutersIterator(args[0]), new DictionaryDimensions(
						WordnetUtils.loadDictionary(args[1])),
				"reuters_noun_probs.ser");

		System.out.println("Built and saved");

		System.out.println("Done");
	}

	private static ResnikDistance load(String path) {
		ResnikDistance rd = ResnikDistanceFactory.loadFromResnikProbabilities(
				"reuters_noun_probs.ser",
				new DictionaryDimensions(WordnetUtils.loadDictionary(path)));
		return rd;
	}

}
