package com.bwinnovations.text.wordnet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.IPointer;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.ISynsetID;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.item.Pointer;
import edu.mit.jwi.morph.WordnetStemmer;

/**
 * Tool for querying the dictionary dimensions
 * @author Laurence Willmore
 *
 */
public class DictionaryDimensions {
	
	public IDictionary dict;
	public WordnetStemmer stemmer;
	public ISynset NOUN_HYP_ROOT;	
	
	
	/**
	 * Default constructor
	 * @param dict
	 */
	public DictionaryDimensions(IDictionary dict) {
		super();
		this.dict = dict;
		IIndexWord idxWord = dict . getIndexWord ("entity", POS. NOUN );
		IWordID wordID = idxWord . getWordIDs ().get (0) ;
		IWord word = dict . getWord ( wordID );
		ISynset currentSyn = word.getSynset();
		List<ISynsetID> hypers = currentSyn.getRelatedMap().get(Pointer.HYPERNYM);
		while(hypers!=null && hypers.size()>0){
			currentSyn = dict.getSynset(hypers.get(0));
		}
		NOUN_HYP_ROOT = currentSyn;
		stemmer = new WordnetStemmer(dict);
	}

	/**
	 * Find the depth of the Noun Hyponym tree.
	 * @return
	 */
	public  int maxDepthNounHyponym(){		
		Map<IPointer, List<ISynsetID>> rmap = NOUN_HYP_ROOT.getRelatedMap();
		int maxDepth=0;
		for(IPointer pointer : rmap.keySet()){
			if(pointer.getName().equals(Pointer.HYPONYM.getName())){
				List<ISynsetID> hyponyms = rmap.get(pointer);
				for(ISynsetID synID : hyponyms){
					int depth = nounDepthHelper(1,synID);
					if(maxDepth < depth){
						maxDepth = depth;
					}
				}			
			}
		}		
		return maxDepth;
	}
	
	private int nounDepthHelper(int currentDepth, ISynsetID synsID){
		ISynset syn = dict.getSynset(synsID);
		Map<IPointer, List<ISynsetID>> rmap = syn.getRelatedMap();
		int maxDepth=currentDepth;
		for(IPointer pointer : rmap.keySet()){
			if(pointer.getName().equals(Pointer.HYPONYM.getName())){
				List<ISynsetID> hyponyms = rmap.get(pointer);
				for(ISynsetID synID : hyponyms){
					int depth = nounDepthHelper(currentDepth+1,synID);
					if(maxDepth < depth){
						maxDepth = depth;
					}
				}			
			}
		}		
		return maxDepth;
	}
	
	int hypDepthOfNounSynset(ISynsetID synid){
		return 0;
	}
	
	/**
	 * Gets all possible {@link ISynset} for the given lemma and POS.
	 * @param lemma
	 * @param pos
	 * @return
	 */
	public List<ISynset> getPossibleSynsets(String lemma, POS pos){
		List<ISynset> result = new ArrayList<ISynset>();
		//TODO: Some of the lemmas seem to be bogus
		Iterator<IWordID> iterator = dict.getIndexWord(lemma,pos).getWordIDs().iterator();
		while(iterator.hasNext()){
			result.add(dict.getSynset(iterator.next().getSynsetID()));
		}
		
		return result;
	}
	
	/**
	 * Gets all possible {@link ISynset} for the given lemma and POS.
	 * @param lemma
	 * @param pos
	 * @return
	 */
	public List<ISynset> getPossibleSynsets(IIndexWord word){
		List<ISynset> result = new ArrayList<ISynset>();
		//TODO: Some of the lemmas seem to be bogus
		Iterator<IWordID> iterator = word.getWordIDs().iterator();
		while(iterator.hasNext()){
			result.add(dict.getSynset(iterator.next().getSynsetID()));
		}
		
		return result;
	}
	
	/**
	 * Check if wordnet contains an indexword for the surface word.
	 * @param word
	 * @return
	 */
	public boolean wordnetContains(String word){
		for(POS pos:POS.values()){
			if(wordnetContains(word,pos))return true;
		}
		return false;
	}
	
	
	/**
	 * Check if wordnet contains an indexword for the surface word and part of speach.
	 * @param word
	 * @param pos
	 * @return
	 */
	public boolean wordnetContains(String word, POS pos){
		List<String> lemmas = stemmer.findStems(word, pos);
		for(String lemma:lemmas){
			if(dict.getIndexWord(lemma, pos)!=null)return true;
		}
		return false;
	}
}
