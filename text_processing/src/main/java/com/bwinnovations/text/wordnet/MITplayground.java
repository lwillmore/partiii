package com.bwinnovations.text.wordnet;

import edu.mit.jwi.IDictionary;

public class MITplayground {
	
	public static void main(String[] args) {
		IDictionary dict = WordnetUtils.loadDictionary(args[0]);
		DictionaryDimensions dd = new DictionaryDimensions(dict);
		System.out.println(dd.maxDepthNounHyponym());
	}
	

}
