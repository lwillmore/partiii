package com.bwinnovations.text.wordnet;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.bwinnovations.text.textpipe.nlpannotation.Token;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.IPointer;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.ISynsetID;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.item.Pointer;
import edu.mit.jwi.morph.WordnetStemmer;

/**
 * Simple edge counting distance metric
 * @author Laurence Willmore
 *
 */
public class EdgeDistance extends GraphBasedDistance{
	
	int nounSynsetDepth;
	
	/**
	 * Default constructor
	 */
	public EdgeDistance(DictionaryDimensions dd){
		super(dd);
		nounSynsetDepth = dd.maxDepthNounHyponym();
	}

	@Override
	public double nouns(String noun1, String noun2) {
		//Get the wordnet words
		List<String> lemmas1 = dd.stemmer.findStems(noun1, POS.NOUN);
		List<String> lemmas2 = dd.stemmer.findStems(noun2, POS.NOUN);
		IIndexWord wnnoun1 = dd.dict.getIndexWord(lemmas1.get(0), POS.NOUN);
		IIndexWord wnnoun2 = dd.dict.getIndexWord(lemmas2.get(0), POS.NOUN);
		
		//Get all the paths to the root for 1
		IWordID wordID = wnnoun1 . getWordIDs ().get (0) ;
		IWord word = dd.dict . getWord ( wordID );
		ISynsetID synid= word.getSynset().getID();	
		List<List<ISynsetID>> paths1 = new ArrayList<List<ISynsetID>>();
		List<ISynsetID> internal1 = new ArrayList<ISynsetID>();
		internal1.add(synid);
		paths1.add(internal1);
		getPathToRoot(paths1, internal1);
		
		//Get all the paths to the root for 2
		IWordID wordID2 = wnnoun2 . getWordIDs ().get (0) ;
		IWord word2 = dd.dict . getWord ( wordID2 );
		ISynsetID synid2= word2.getSynset().getID();
		List<List<ISynsetID>> paths2 = new ArrayList<List<ISynsetID>>();
		List<ISynsetID> internal2 = new ArrayList<ISynsetID>();
		internal2.add(synid2);
		paths2.add(internal2);
		getPathToRoot(paths2,internal2);
			
		//Find the subsumer with the lowest amount of edges
		int edges = 2*nounSynsetDepth;
		for(List<ISynsetID> path : paths1){
			for(List<ISynsetID> path2: paths2){
				int e = getEdgeDistance(path,path2);
				if(e<edges)edges=e;
			}
		}
		return edges;
	}

	private int getEdgeDistance(List<ISynsetID> path, List<ISynsetID> path2) {
		int i1 = path.size()-1;
		int i2 = path2.size()-1;
		boolean match = false;
		while(!match && i1>=0 && i2 >=0){
			ISynset s1 = dd.dict.getSynset(path.get(i1--));
			ISynset s2 = dd.dict.getSynset(path2.get(i2--));
			match=!(s1.hashCode()==s2.hashCode());
		}
		if(match==false)return 0;
		return i1+i2+4;
	}

	@Override
	public double verbs(String verb1, String verb2) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * Small test main
	 * @param args
	 */
	public static void main(String[] args) {
		//EdgeDistance e = new EdgeDistance(args[0]);
		//System.out.println(e.nouns("dog", "wolf"));
	}

	@Override
	public double nouns(ISynsetID noun1, ISynsetID noun2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double verbs(ISynsetID verb1, ISynsetID verb2) {
		// TODO Auto-generated method stub
		return 0;
	}


}
